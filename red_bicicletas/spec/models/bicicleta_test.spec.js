//Importando el modulo
var Bicicleta = require('../../models/bicicleta');
//Para correr los test usamos npm test


//esta es una funcion que se coloca antes de que se ejecuten todas las pruebas para evitar repetición de código
beforeEach(()=>{
    Bicicleta.allBicis=[];
});

//Testear que las bicicletas comiencen en cero
//call back se llama como una funcion landa en el nuevo emac 6
describe ('Bicicleta.allBicis', ()=>{
    it ('comienza vacía', ()=>{
        expect(Bicicleta.allBicis.length).toBe(0);

    });
});

//Testear que se agregue una bicicleta

describe ('Bicicleta.add', ()=>{
    it ('agregamos una' , ()=>{
        //Precondicion
        expect(Bicicleta.allBicis.length).toBe(0);

        var a= new Bicicleta(1, 'rojo', 'urbana', [-34.6012424,-58.3861497]);
        Bicicleta.add(a);
        //Despues de insertar la bicicleta la longitud sea uno
        expect(Bicicleta.allBicis.length).toBe(1);
        //Despues validar que la unica bicicleta insertada sea la a
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

//Vamos a proar el findby

describe('Bicicletafind by', ()=>{
    it('debe devolver la bicicleta con el id proprocionaod por ejemplo 1', ()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1, 'verde', 'urbana');
        var aBici2 = new Bicicleta(2, 'rojo', 'montana');
        //Agregando al arreglo bicicleta las siguientes bicicletas
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);

      
        var targetBicicleta= Bicicleta.findById(1);
        expect(targetBicicleta.id).toBe(1);
        expect(targetBicicleta.color).toBe('verde');
        expect(targetBicicleta.modelo).toBe('urbana');


    });
});
