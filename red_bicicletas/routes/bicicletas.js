//Importar la librería de express por que utilizaremos el modulo de rutas del framework
var express = require('express');
//Definimos el uso de la libreria Router
var router=express.Router();
//importando el controller, cada uno de los metodos del controler se llaman acciones
var bicicletaController= require('../controllers/bicicleta');
//Definiendo rutas

//1. Definiendo la ruta base 
router.get('/',bicicletaController.bicicleta_list);

//2. Definiendo la ruta de crear una bicicleta
router.get('/create', bicicletaController.bicicleta_create_get);
router.post('/create', bicicletaController.bicicleta_create_post);
//Ruta para el delete
router.post('/:id/delete', bicicletaController.bicicleta_delete_post);

//Actualizacion de la bicicleta creamos las rutas
router.get('/:id/update', bicicletaController.bicicleta_update_get);
router.post('/:id/update', bicicletaController.bicicleta_update_post);

//Siempre es importante exportar las rutas
module.exports=router;