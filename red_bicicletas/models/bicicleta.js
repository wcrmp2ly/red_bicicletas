var Bicicleta = function (id, color, modelo, ubicacion){
    //Esto es como si fuera el constructor
    this.id=id;
    this.color=color;
    this.modelo=modelo;
    this.ubicacion=ubicacion;
}

Bicicleta.prototype.toString= function(){
    return 'id:' + this.id + "color: " +this.color;
}
//Donde guardaremos las bicicletas, es un arreglo.
Bicicleta.allBicis =[];

//Metodo para agregar una bicicleta al arreglo
Bicicleta.add = function(aBici){
     Bicicleta.allBicis.push(aBici)
}

//Por simetria en el diseño debemos contar con metodo de quitar.

//Metodo para buscar el id de la bicicleta
Bicicleta.findById=function(aBiciId){
    //Se utiliza un metodo de la coleccion de las bicicletas
    var aBici=Bicicleta.allBicis.find(x =>x.id == aBiciId);
    if (aBici)
        return aBici
    else
       //La interpolacion solo funciona con las comillas inversas acento grave
       throw new Error (`No existe una bicicleta con el id ${aBiciID}`);    
}

//Remove Method

Bicicleta.removeById=function(aBiciId){
    console.log("Entrando a Bicleta.removeById con el id aBiciId--> " +aBiciId);
     for (var i=0; i< Bicicleta.allBicis.length; i++){
        if (Bicicleta.allBicis[i].id==aBiciId){
            Bicicleta.allBicis.splice(i, 1);
            console.log("Eliminando el id "+i + "aBicid es" +aBiciId);
            break;
        }
    }
}


//  L.marker([-34.6012424,-58.3881497]).addTo(map);
  //  L.marker([-34.596932,-58.340207]).addTo(map)
//var a=new Bicicleta(1, 'rojo', 'urbana', [-34.6012424,-58.3861497]);
//var b=new Bicicleta(2, 'blanca', 'urbana',[-34.596932,-58.3808287]);

//Agregando las bicicletas en el arreglo para no usar base de datos
//Bicicleta.add(a);
//Bicicleta.add(b);
//exportamos el objeto del modelo
module.exports = Bicicleta;
