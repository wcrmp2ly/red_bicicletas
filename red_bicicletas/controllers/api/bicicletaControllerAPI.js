//Importar el modelo de la bicicleta
var Bicicleta = require('../../models/bicicleta');

//metodo de la lista de bicicletas
exports.bicicleta_list=function(req,res){
    res.status(200).json({bicicletas: Bicicleta.allBicis});
}

//metodo de la lista de creacion
exports.bicicleta_create= function(req,res){
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici);
    res.status(200).json({
        bicicleta: bici
    });
}

//metod para borrar
exports.bicicleta_delete=function(req,res){
    Bicicleta.removeById(req.body.id);
    res.status(204).send();

}