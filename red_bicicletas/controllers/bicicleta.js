//Importar el modulo de bicicleta
var Bicicleta= require('../models/bicicleta')


exports.bicicleta_list= function(req, res){
    //Mostrar la lista de bicicletas
    //Indica el nombre del template en la primera sección y la variable que trae todo el resultado de las bicis
  res.render('bicicletas/index',{bicis: Bicicleta.allBicis});
}

//Controlador para crear una bicicleta pero en la respuesta get primer momento: solicitud del create
exports.bicicleta_create_get=function(req, res){
     //Render de la vista de bicicletas para crear
     res.render('bicicletas/create');
}
//Controlador para el segundo momento
exports.bicicleta_create_post=function(req, res){
    //Atributos vienen del body por lo que cada uno es definido con identificador
    var bici=new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion=[req.body.lat, req.body.lng];
    Bicicleta.add(bici);
    //Redireccionar cuando terminamos a la página principal de bicicletas
    res.redirect('/bicicletas');
}

exports.bicicleta_update_get=function(req, res){
    //objeto bici lo vamos a buscar por parametro en el url
    var bici = Bicicleta.findById(req.params.id);
    res.render('bicicletas/update', {bici});

}


exports.bicicleta_update_post=function(req, res){
    var bici = Bicicleta.findById(req.params.id);
    bici.id=req.body.id;
    bici.color=req.body.color;
    bici.modelo=req.body.modelo;
    bici.ubicacion=[req.body.lat, req.body.lng];
    res.redirect('/bicicletas');


}

exports.bicicleta_delete_post=function(req, res){
    
    Bicicleta.removeById(req.body.id);
    console.log('Entrando en el controler de borrar')

    res.redirect('/bicicletas');

}

