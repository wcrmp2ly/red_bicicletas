var map = L.map('main_map').setView([-34.6012424, -58.3861497], 13);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    }).addTo(map);

    //Inicializacion de los puntos del mapa de forma manual
   //L.marker([-34.6012424,-58.3881497]).addTo(map);
   //L.marker([-34.596932,-58.340207]).addTo(map);
    //L.marker([-34.599564,-58.3778777], 'stesting').addTo(map);
//console.log("Verificando el contenido");
//La funcion ajax a diferencia del video tenemos que usar el done en lugar de la función sucess
$.ajax({
    dataType: "json",
    url: "api/v1/bicicletas"
}).done( function(result){
     result.bicicletas.forEach(function(bici){
     L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
    })
});